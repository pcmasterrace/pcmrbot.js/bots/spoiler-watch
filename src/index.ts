import { ServiceBroker } from "moleculer";

import SpoilerWatchService from "@services/main";

/**
 * Automatically loads all available services in this package
 * @param {ServiceBroker} broker 
 */
export default function registerAllSpoilerWatchServices(broker: ServiceBroker): void {
    broker.createService(SpoilerWatchService);
}

export {
    SpoilerWatchService
}