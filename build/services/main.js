"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moleculer_1 = require("moleculer");
class SpoilerWatchService extends moleculer_1.Service {
    constructor(broker) {
        super(broker);
        this.parseServiceSchema({
            name: "spoilerWatch",
            version: 3,
            dependencies: [
                { name: "reddit.post", version: 3 },
                { name: "reddit-rt.modlog", version: 3 },
                { name: "slack.web", version: 3 },
                { name: "slack.utilities", version: 3 }
            ],
            settings: {
                reason: process.env.BOT_SPOILERWATCH_REASON,
                channel: process.env.BOT_SPOILERWATCH_CHANNEL
            },
            events: {
                "v3.reddit-rt.modlog.removecomment": this.eventHandler,
                "v3.reddit-rt.modlog.removelink": this.eventHandler
            }
        });
    }
    async eventHandler(payload) {
        if (payload.details.includes(this.settings.reason)) {
            const post = await this.broker.call("v3.reddit.post.get", {
                postId: payload.target_fullname
            });
            const blocks = await this.broker.call("v3.slack.utilities.generateSlackBlocks", { post });
            // Send message to Slack
            await this.broker.call("v3.slack.web.postMessage", {
                channel: this.settings.channel,
                blocks: [{
                        type: "section",
                        text: {
                            type: "mrkdwn",
                            text: `*Notice*: <https://reddit.com/u/${post.author}|${post.author}> may have posted a spoiler in their latest post`
                        }
                    }],
                attachments: [{ blocks }]
            });
        }
    }
}
exports.default = SpoilerWatchService;
