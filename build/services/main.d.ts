import { Service } from "moleculer";
export default class SpoilerWatchService extends Service {
    constructor(broker: any);
    eventHandler(payload: any): Promise<void>;
}
